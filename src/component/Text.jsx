import React, {useEffect, useState} from "react";

export default function Text(props) {
    const [color, setColor] = useState('');
    const [fontSize, setFontSize] = useState(0);

    useEffect(() => {
        setColor(props.color);
        setFontSize(props.fontSize);
    }, [props]);

    return (
        <div className="col-md-12">
            <div className="card">
                <div className="card-header alert-danger font-weight-bold">
                    Màu chữ: {color}  - Font-size: {fontSize} px
                </div>
                <div className="card-body">
                    <li className="list-group-item">
                        <span style={{color: color, fontSize: fontSize}}>
                            Nội dung hiển thị cho cài đặt
                        </span>
                    </li>
                </div>
            </div>
        </div>
    );
}

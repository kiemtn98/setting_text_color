import React, {useState, useEffect} from "react";
import {Button} from "react-bootstrap";
import { FONT_SIZE } from "../constant/Setting";

export default function FontSize(props) {
    const [fontSize, setFontSize] = useState(0);

    useEffect(
        () => {
            setFontSize(props.fontSize);
        }, [props]);

    const handleClick = (number, type) => {
        if (type === 'increment') {
            setFontSize(fontSize + number);
            return props.changeFontSize(fontSize + number);
        }

        if (type === 'decrement') {
            setFontSize(fontSize - number);
            return props.changeFontSize(fontSize - number);
        }

        setFontSize(FONT_SIZE);
        return props.changeFontSize(FONT_SIZE);

    };

    return (
        <div className="col-md-6 display-inline-block">
            <div className="card text-center">
                <div className="card-header alert-success font-weight-bold">
                    Font size
                </div>
                <div className="card-body">
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">Font size hiện tại: {fontSize} px</li>
                        <li className="list-group-item">
                            <Button variant="warning"
                                    onClick={() => {handleClick(1, 'increment')}}
                                    className="mr-right-10px font-weight-bold">+
                            </Button>
                            <Button variant="success"
                                    onClick={() => {handleClick(1, 'decrement')}}
                                    className="mr-right-10px font-weight-bold">-
                            </Button>
                            <Button variant="danger"
                                    onClick={() => {handleClick(1, 'reset')}}
                            >Reset</Button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

import React, {useState, useEffect} from "react";
import {Button} from "react-bootstrap";

export default function ColorPicker(props) {
    const [color, setColor] = useState('');

    useEffect(
        () => {
            setColor(props.color);
        }, [props]);

    const handleClick = (color) => {
        setColor(color);
        props.changeColor(color);
    };

    return (
        <div className="col-md-6 display-inline-block">
            <div className="card text-center">
                <div className="card-header alert-info font-weight-bold">
                    Color Picker
                </div>
                <div className="card-body">
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">Màu hiện tại:
                            <span style={{color: color}}> {color}</span>
                        </li>
                        <li className="list-group-item">
                            <Button variant="danger"
                                    onClick={() => {handleClick('red')}}
                                    className={color === 'red' ? 'color-active' : ''}
                            >Đỏ</Button>{' '}
                            <Button variant="primary"
                                    onClick={() => {handleClick('blue')}}
                                    className={color === 'blue' ? 'color-active' : ''}
                            >Xanh dương</Button>{' '}
                            <Button variant="secondary"
                                    onClick={() => {handleClick('black')}}
                                    className={color === 'black' ? 'color-active' : ''}
                            >Đen</Button>{' '}
                            <Button variant="success"
                                    onClick={() => {handleClick('green')}}
                                    className={color === 'green' ? 'color-active' : ''}
                            >Xanh lá cây</Button>{' '}
                            <Button variant="warning"
                                    onClick={() => {handleClick('yellow')}}
                                    className={color === 'yellow' ? 'color-active' : ''}
                            >Vàng</Button>{' '}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

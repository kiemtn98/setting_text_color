import React, {useState} from 'react';
import '../index.css';
import { FONT_SIZE, COLOR } from "../constant/Setting";
import ColorPicker from "./ColorPicker";
import FontSize from "./FontSize";
import Text from "./Text";

export default function SettingTextColor() {
    const [color, setColor] = useState(COLOR);
    const [fontSize, setFontSize] = useState(FONT_SIZE);

    const changeColor = (color) => {
        setColor(color);
    };

    const changeFontSize = (fontSize) => {
        setFontSize(fontSize);
    };

    return (
        <React.Fragment>
            <div className="container mg-top-5pc">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card text-center">
                            <div className="card-header font-weight-bold alert-warning text-uppercase">
                                Setting color picker
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mg-top-2pc">
                    <div className="col-md-12">
                        <ColorPicker color={color} changeColor={(color) => {changeColor(color)}} />
                        <FontSize fontSize={fontSize} changeFontSize={(fontSize) => {changeFontSize(fontSize)}} />
                    </div>
                </div>

                <div className="row mg-top-2pc">
                    <Text color={color} fontSize={fontSize}/>
                </div>
            </div>
        </React.Fragment>
    );

}
